
const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const { ERROR:httpError, SUCCESS:http } = require('../../../helpers/http-status/status_code');
const { result } = require('validate.js');

// const postDataLogin = async (req, res) => {
//   const payload = req.body;
//   const validatePayload = validator.isValidPayload(payload, commandModel.login);
//   const postRequest = async (result) => {
//     if (result.err) {
//       return result;
//     }
//     return commandHandler.postDataLogin(result.data);
//   };

//   const sendResponse = async (result) => {
//     (result.err) ? wrapper.response(res, 'fail', result, 'Login User')
//       : wrapper.response(res, 'success', result, 'Login User', http.OK);
//   };
//   sendResponse(await postRequest(validatePayload));
// };

const getUser = async (req, res) => {
  const { idUser } = req.params;
  const getData = async () => queryHandler.getUser(idUser);
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Get User', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get User', http.OK);
  };
  sendResponse(await getData());
};

const getProduct = async (req, res) => {
  const getData = async () => queryHandler.getProduct();
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Get Product', httpError.NOT_FOUND)
      : wrapper.response(res, 'success', result, 'Get Product', http.OK);
  };
  sendResponse(await getData());
};

// const getAllBook = async (req, res) => {
//   const getData = async () => queryHandler.getAllBook();
//   const sendResponse = async (result) => {
//     (result.err) ? wrapper.response(res, 'fail', result, 'Get Book', httpError.NOT_FOUND)
//       : wrapper.response(res, 'success', result, 'Get Book', http.OK);
//   };
//   sendResponse(await getData());
// };

// const getProduct = async (req, res) => {
//   const ( idProduct ) = req.params;
//   const getData = async () => queryHandler.getProduct(idProduct);
//   const sendResponse = async (result) => {
//     (result.err) ? wrapper.res(res, 'gagal', result, 'berhasil ambil data', httpError.NOT_FOUND)
//     : wrapper.response(res, 'sukses', result, 'berhasil ambil data', http.OK);
//   };
//   sendResponse(await getData());
// };

// const registerUser = async (req, res) => {
//   const payload = req.body;
//   const validatePayload = validator.isValidPayload(payload, commandModel.register);
//   const postRequest = async (result) => {
//     if (result.err) {
//       return result;
//     }
//     return commandHandler.registerUser(result.data);
//   };
//   const sendResponse = async (result) => {
//     /* eslint no-unused-expressions: [2, { allowTernary: true }] */
//     if (result.err)  {
//       wrapper.response(res, 'fail', result, 'Register User', httpError.CONFLICT);
//     } else {
//       wrapper.response(res, 'success', result, 'Register User', http.OK);
//     }
//   };
//   sendResponse(await postRequest(validatePayload));
// };

const postProduct = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.product);
  const postRequest = async (result) => {
    if (result.err){
      return result;
    }
    return commandHandler.getProduct(result.data);
  };

  const sendResponse = async(result) => {
    if(result.err){
      wrapper.response(res, 'fail', result, 'Product', httpError.CONFLICT);
    }else{
      wrapper.response(res, 'succes',result, 'Product', http.OK);
    }
  }
  sendResponse(await postRequest(validatePayload));
}

const updateProduct = async (req, res) => {
  const payload = req.body;
  const {idProduct} = req.params;
  const validatePayload = validator.isValidPayload(payload, commandModel.product);
  const updateData = async (result) => {
    if (result.err){
      return result;
    }
    return commandHandler.updateProduct(idProduct,payload);
  };
  const sendResponse = async (result) => {
    if (result.err)  {
      wrapper.response(res, 'fail', result, 'Update User', httpError.CONFLICT);
    } else {
      wrapper.response(res, 'success', result, 'Update User', http.OK);
    }
  };
  sendResponse(await updateData(validatePayload));
};



module.exports = {
  // postDataLogin,
  // getUser,
  // registerUser,
  // updateUser,
  postProduct,
  getProduct,
  updateProduct
};
