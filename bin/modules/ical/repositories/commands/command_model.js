const joi = require('joi'); 


const login = joi.object({
  username: joi.string().required(),
  password: joi.string().required(),

  isActive : joi.boolean().default(true, 'Example If Need Default Value')
});

const register = joi.object({
  name: joi.string().required(),
  nohp: joi.string().required(),
  password: joi.string().required(),


  isActive : joi.boolean().default(true, 'Example If Need Default Value')
});

const product = joi.object({
  foto : joi.string().required(),
  nama_produk : joi.string().required(),
  kode_produk : joi.string().required(),
  stok : joi.number().required(),
  bobot_produk : joi.number().required(),
  harga : joi.number().required(),
  informasi_produk : joi.string().required()
})

module.exports = {
  login,
  register,
  product
};
