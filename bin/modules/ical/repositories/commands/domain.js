const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const jwtAuth = require('../../../../auth/jwt_auth_helper');
const commonUtil = require('../../../../helpers/utils/common');
const logger = require('../../../../helpers/utils/logger');
const { NotFoundError, UnauthorizedError, ConflictError } = require('../../../../helpers/error');

const algorithm = 'aes-256-ctr';
const secretKey = 'Dom@in2018';

class Product {

  constructor(db){
    this.command = new Command(db);
    this.query = new Query(db);
  }
  
  async postProduct(payload) {
    const {foto, nama_produk, kode_produk,stok,bobot_produk,harga,informasi_produk} = payload;
    const data = {
      foto,
      nama_produk,
      kode_produk,
      stok,
      bobot_produk,
      harga,
      informasi_produk,
    };
    const {data:result} = await this.command.insertProduct(data);
    return wrapper.data(result);
  }

  
  async updateOneProduct(idProduct,payload){
    const {data:result} = await this.command.updateOneProduct(idProduct, payload);
    return wrapper.data(result);
  }


  async updateOneUser(userId,payload){
    const { name, username, password, isActive } = payload;
    const chiperPwd = await commonUtil.encrypt(password, algorithm, secretKey);
    const data = {
      name,
      username,
      password: chiperPwd,
      isActive
    };
    const {data:result} = await this.command.updateOneUser(userId, data);
    return wrapper.data(result);
  }

}

module.exports = Product;
