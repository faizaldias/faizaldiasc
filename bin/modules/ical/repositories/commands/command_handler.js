
const Product = require('./domain');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mongo(config.get('/mongoDbUrl'));
const product = new Product(db);

// const postDataLogin = async (payload) => {
//   const postCommand = async payload => ical.generateCredential(payload);
//   return postCommand(payload);
// };

// const registerUser = async (payload) => {
//   const postCommand = async payload => ical.register(payload);
//   return postCommand(payload);
// };

const getProduct = async (payload) => {
  const getCommand = async payload => product.postProduct(payload);
  return getCommand(payload);
}


const updateProduct = async (idProduct,payload) => {
  const updateCommand = async payload => product.updateOneProduct(idProduct,payload);
  return updateCommand(payload);
};

module.exports = {
  // postDataLogin,
  // registerUser,
  // updateUser,
  getProduct,
  updateProduct
};
