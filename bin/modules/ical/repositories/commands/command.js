const ObjectId = require('mongodb').ObjectID;
class Command {

  constructor(db) {
    this.db = db;
    this.db.setCollection('product');
  }

  async insertOneUser(document){
    const result = await this.db.insertOne(document);
    return result;
  }
  
  async insertProduct(document){
    const result = await this.db.insertOne(document);
    return result;
  }

  async updateOneProduct(idProduct,document){
    const result = await this.db.updateOne({ _id: ObjectId(idProduct) }, document);
    return result;
  }
}

module.exports = Command;
