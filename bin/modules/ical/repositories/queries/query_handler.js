
const Product = require('./domain');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mongo(config.get('/mongoDbUrl'));
const product = new Product(db);

const getUser = async (userId) => {
  const getData = async () => {
    const result = await user.viewUser(userId);
    return result;
  };
  const result = await getData();
  return result;
};

const getProduct = async () => {
  const getData = async () => {
    const result = await product.viewAllProduct();
    return result;
  };
  const result = await getData();
  return result;
};
module.exports = {
  getUser,
  getProduct
};
