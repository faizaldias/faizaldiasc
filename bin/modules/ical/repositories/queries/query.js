
const ObjectId = require('mongodb').ObjectId;
const wrapper = require('../../../../helpers/utils/wrapper');

class Query {

  constructor(db) {
    this.db = db;
    this.db.setCollection('product');
  }

  async findOneUser(parameter) {
    const recordset = await this.db.findOne(parameter);
    return recordset;
  }

  async findAll() {
    const ctx = 'mongodb-findAllProduct';
    const db = await this.db.customQuery(ctx);
    try { const recordset = await db.find({}).toArray();
      return wrapper.data(recordset);
    } catch (err) {
      return wrapper.error(`Error findAll Mongo ${err.message}`);
    }
  }

  async findById(id) {
    const parameter = {
      _id: ObjectId(id)
    };
    const recordset = await this.db.findOne(parameter);
    return recordset;
  }

}

module.exports = Query;
