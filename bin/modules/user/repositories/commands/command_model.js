const joi = require('joi'); 


const login = joi.object({
  username: joi.string().required(),
  password: joi.string().required(),

  isActive : joi.boolean().default(true, 'Example If Need Default Value')
});

const register = joi.object({
  name: joi.string().required(),
  nohp: joi.string().required(),
  password: joi.string().required(),


  isActive : joi.boolean().default(true, 'Example If Need Default Value')
});

const product = joi.object({
  deskripsi: joi.string().required(),
  harga:joi.string().required(),
  berat:joi.string().required()
})

module.exports = {
  login,
  register,
  product,
};
